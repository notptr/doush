#include <stdio.h> //printf
#include <string.h> //strlen
#include <stdlib.h> //exit
#include <unistd.h> //syscalls

#include <sys/wait.h>
#include <sys/types.h>



#define MAXARGS 128

struct command 
{
    int argc; //number of args
    char *argv[MAXARGS]; //arg list
    enum builtin_t 
    {
        NONE, 
        QUIT, 
        JOBS, 
        BG, 
        FG,
        CD
    } builtin;
};

enum builtin_t parse_builtin (struct command *cmd) 
{
    if ( !strcmp(cmd->argv[0], "quit") || !strcmp(cmd->argv[0], "exit"))
    {
        return QUIT;
    }
    else if ( !strcmp(cmd->argv[0], "jobs") )
    {
        return JOBS;
    } 
    else if ( !strcmp(cmd->argv[0], "bg") )
    {
        return BG;
    }
    else if ( !strcmp(cmd->argv[0], "fg") )
    {
        return FG;
    }
    else if ( !strcmp(cmd->argv[0], "cd") )
    {
        return CD;
    }
    else
    {
        return NONE;
    }
}

int MAXLINE = 1024;
int run = 1;
char prompt[] = ">doush:) ";

void run_system_command ( struct command *cmd, int bg ) 
{
    pid_t child_pid;

    //fork
    if ( (child_pid = fork()) < 0 )
        fprintf( stderr, "Fork error\n");
    else if ( child_pid == 0 )
    {
        //I'm the child and could run a command
        //execvp

        if ( execvp(cmd->argv[0], cmd->argv ) < 0 )
        {
            printf("%s: Command not found\n", cmd->argv[0]);
            exit(0);
        }
    }
    else 
    {
        if ( bg )
            printf("Child in background [%d]\n", child_pid);
        else
            wait(&child_pid);
    }
}

void run_builtin_command ( struct command *cmd, int bg )
{
    switch ( cmd->builtin )
    {
        case QUIT:
            run = 0;
            break;
        case JOBS:
            printf("TODO: job\n");
            break;
        case BG:
            printf("TODO bg\n");
            break;
        case FG:
            printf("TODO fg\n");
            break;
        case CD:
            if ( chdir(cmd->argv[1]) == -1 )
                fprintf(stderr, "Couldn't locate: %s\n", cmd->argv[1]);
            break;
        default:
            fprintf(stderr, "Unkown builtin command\n");
    }
}


static int parse ( const char *cmdline, struct command *cmd )
{
    char array[MAXLINE]; //local copy of command
    const char delima[10] = " \t\r\n"; //argument delimaitere
    char *line = array; //ptr travarses the command line
    char *token; //ptr to the end of the current arg
    char *endline; //pte to the end of the commandline
    int is_bg; //background job


    if ( cmdline == NULL )
        fprintf(stderr, "commandline is null\n");

    (void) strncpy(line, cmdline, MAXLINE);
    endline = line + strlen(line);


    //build arglist
    cmd->argc = 0;

    while ( line < endline )
    {
        //skip delim
        line += strspn ( line, delima );

        if ( line >= endline ) break;

        //find a token delim
        token = line + strcspn ( line, delima );
        
        //terminate the token
        *token = '\0';

        //record token
        cmd->argv[cmd->argc++] = line;

        //check if the argv is full 
        if ( cmd->argc >= MAXARGS - 1 ) break;

        line = token + 1;

    }

    //argument list must end with a null pointer
    cmd->argv[cmd->argc] = NULL;

    if ( cmd->argc == 0 ) //ignore blank line
        return 1;

    cmd->builtin = parse_builtin(cmd);

    //run the job in the backgorund
    if ( (is_bg = (*cmd->argv[cmd->argc-1] == '&') ) != 0)
        cmd->argv[--cmd->argc] = NULL;

    return is_bg;
}

static void eval ( char *cmdline )
{
    int bg;
    struct command cmd;

    printf("Evaluating '%s'\n", cmdline);
    bg = parse(cmdline, &cmd);

    printf("Found command %s\n", cmd.argv[0]);

    //-1 is error
    if ( bg == -1 ) return;

    if ( cmd.argv[0] == NULL ) return;

    if ( cmd.builtin == NONE ) 
        run_system_command( &cmd, bg );
    else
        run_builtin_command( &cmd, bg );
}



int main ( int argc, char **argv ) 
{
    char cmdline[MAXLINE]; //buffer

    while(run)
    {
        printf("%s", prompt);

        if ( (fgets(cmdline, MAXLINE, stdin) == NULL) && ferror(stdin) )
            fprintf (stderr, "Fgets error");

        if ( feof(stdin) ) 
        {
            printf("\n");
            exit(0);
        }

        //remove the trailing newline
        cmdline[strlen(cmdline) - 1] = '\0';


        eval(cmdline);
    }

    return 0;
}
